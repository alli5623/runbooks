<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->
#  Camoproxy Service

* **Responsible Teams**:
  * [infrastructure-coreinfra](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/). **Slack Channel**: [#production](https://gitlab.slack.com/archives/production)
* **General Triage Dashboard**: https://dashboards.gitlab.net/d/26q8nTzZz/service-platform-metrics?from=now-6h&to=now&var-prometheus_ds=Global&var-environment=gprd&var-type=camoproxy&orgId=1
* **Alerts**: https://alerts.gprd.gitlab.net/#/alerts?filter=%7Btype%3D%22camoproxy%22%2C%20tier%3D%22sv%22%7D
* **Label**: gitlab-com/gl-infra/production~"Service:Camoproxy"

## Logging

* [Camoproxy](https://log.gprd.gitlab.net/goto/f86d35a17e46e0de9d8454b3a5d4387f)

## Troubleshooting Pointers

* [../uncategorized/camoproxy.md](../uncategorized/camoproxy.md)
* [../uncategorized/upgrade-camoproxy.md](../uncategorized/upgrade-camoproxy.md)
<!-- END_MARKER -->
